## 🚧Arquitetura FrontEnd - NextJS 

Esse projeto visa adquirir boas práticas na construção de projetos utilizando as ferramentas: 
- ✅ NextJS 
- ✅ Typescript

E como usando os comandos YARN como sistema de empacotamento.

Esse projeto segue o curso **[Alura: nextjs-arquitetura-front-end](https://cursos.alura.com.br/course/nextjs-arquitetura-front-end)** como base. 

![gif](https://media2.giphy.com/media/L1R1tvI9svkIWwpVYr/200w.webp?cid=ecf05e474hpb6ehak4zv279drasn2hqeqzrrnrydrpkvxq52&rid=200w.webp&ct=g)

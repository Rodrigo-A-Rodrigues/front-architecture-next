import React from 'react';
import { sum } from '@alura/utils/math/sum';

export default function Home() {
  return (
    <>
      <h1>Home</h1>
      <p>Import module @alura/utils/math/sum sum(2,2): {sum(2,2)}</p>
    </>
  )
}
